import { Component } from '@angular/core';
import { NavController, NavParams, ActionSheetController, ToastController } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { Geolocation } from '@ionic-native/geolocation';
import { NativeGeocoder, NativeGeocoderReverseResult } from '@ionic-native/native-geocoder';

import { HomePage } from '../home/home';
import { Foto } from '../../commons/Foto';

import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import firebase from 'firebase';



@Component({
  selector: 'page-new-photo',
  templateUrl: 'new-photo.html',
})
export class NewPhotoPage {

  
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private sheetCtrl: ActionSheetController,
    private camara: Camera,
    private geocoder: NativeGeocoder,
    private geolocation: Geolocation,
    private database: AngularFirestore,
    private toastCtrl: ToastController
  ) {
    
  }

  
}
