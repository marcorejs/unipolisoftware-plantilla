import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { Camera } from '@ionic-native/camera';
import { NativeGeocoder } from '@ionic-native/native-geocoder';
import { Geolocation } from '@ionic-native/geolocation';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { NewPhotoPage } from '../pages/new-photo/new-photo';
import { ViewPhotoPage } from '../pages/home/verFoto';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';

export const config = {
    apiKey: "AIzaSyDXRqxOfplZLgjqJQkeLz4pkpPz9NOnl5M",
    authDomain: "unipolisocial.firebaseapp.com",
    databaseURL: "https://unipolisocial.firebaseio.com",
    projectId: "unipolisocial",
    storageBucket: "unipolisocial.appspot.com",
    messagingSenderId: "290761032372"
};


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    NewPhotoPage,
    ViewPhotoPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(config),
    AngularFirestoreModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    NewPhotoPage,
    ViewPhotoPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Camera,
    Geolocation,
    NativeGeocoder,
  ]
})
export class AppModule {}
